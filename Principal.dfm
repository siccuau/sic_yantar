﻿object UPrincipal: TUPrincipal
  Left = 0
  Top = 0
  BorderStyle = bsNone
  Caption = 'Yantar Comandas'
  ClientHeight = 950
  ClientWidth = 1623
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  WindowState = wsMaximized
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 925
    Width = 1623
    Height = 25
    Panels = <
      item
        Text = '<< AD2007 >>'
        Width = 80
      end>
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 1604
    Height = 919
    ActivePage = TabSheet2
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnChange = PageControl1Change
    object TabSheet1: TTabSheet
      Caption = 'Comandas Pendientes'
      object Label9: TLabel
        Left = 530
        Top = 828
        Width = 94
        Height = 25
        Caption = 'Comedor'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label10: TLabel
        Left = 675
        Top = 828
        Width = 97
        Height = 25
        Caption = 'Domicilio'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object lblDomicilio: TLabel
        Left = 778
        Top = 828
        Width = 28
        Height = 25
        Caption = '    '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = False
      end
      object lblComedor: TLabel
        Left = 631
        Top = 828
        Width = 28
        Height = 25
        Caption = '    '
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Transparent = False
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1463
        Height = 855
        Caption = 'Pendientes'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        object Label1: TLabel
          Left = 9
          Top = 16
          Width = 6
          Height = 24
          Alignment = taCenter
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -20
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          Transparent = False
        end
        object Label2: TLabel
          Left = 369
          Top = 16
          Width = 6
          Height = 24
          Alignment = taCenter
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -20
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          Transparent = False
        end
        object Label3: TLabel
          Left = 725
          Top = 16
          Width = 6
          Height = 24
          Alignment = taCenter
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -20
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          Transparent = False
        end
        object Label4: TLabel
          Left = 1081
          Top = 16
          Width = 6
          Height = 24
          Alignment = taCenter
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -20
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          Transparent = False
        end
        object Label5: TLabel
          Left = 37
          Top = 471
          Width = 6
          Height = 24
          Alignment = taCenter
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -20
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          Transparent = False
        end
        object Label6: TLabel
          Left = 369
          Top = 407
          Width = 6
          Height = 24
          Alignment = taCenter
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -20
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          Transparent = False
        end
        object Label7: TLabel
          Left = 725
          Top = 407
          Width = 6
          Height = 24
          Alignment = taCenter
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -20
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          Transparent = False
        end
        object Label8: TLabel
          Left = 1081
          Top = 407
          Width = 6
          Height = 24
          Alignment = taCenter
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -20
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          Transparent = False
        end
        object Texto1: TLabel
          Left = 21
          Top = 24
          Width = 6
          Height = 24
          Alignment = taCenter
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -20
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          Transparent = False
          Visible = False
        end
        object Texto2: TLabel
          Left = 377
          Top = 24
          Width = 6
          Height = 24
          Alignment = taCenter
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -20
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          Transparent = False
          Visible = False
        end
        object Texto3: TLabel
          Left = 733
          Top = 24
          Width = 6
          Height = 24
          Alignment = taCenter
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -20
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          Transparent = False
          Visible = False
        end
        object Texto4: TLabel
          Left = 1089
          Top = 24
          Width = 6
          Height = 24
          Alignment = taCenter
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -20
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          Transparent = False
          Visible = False
        end
        object Texto5: TLabel
          Left = 21
          Top = 415
          Width = 6
          Height = 24
          Alignment = taCenter
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -20
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          Transparent = False
          Visible = False
        end
        object Texto6: TLabel
          Left = 377
          Top = 415
          Width = 6
          Height = 24
          Alignment = taCenter
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -20
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          Transparent = False
          Visible = False
        end
        object Texto7: TLabel
          Left = 733
          Top = 415
          Width = 6
          Height = 24
          Alignment = taCenter
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -20
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          Transparent = False
          Visible = False
        end
        object Texto8: TLabel
          Left = 1089
          Top = 415
          Width = 6
          Height = 24
          Alignment = taCenter
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -20
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          Transparent = False
          Visible = False
        end
        object ListView1: TListView
          Left = 13
          Top = 40
          Width = 350
          Height = 361
          BorderStyle = bsNone
          Columns = <
            item
              Caption = 'U.'
              Width = 30
            end
            item
              Caption = 'Articulo'
              Width = 295
            end>
          ColumnClick = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -19
          Font.Name = 'Arial Narrow'
          Font.Style = [fsBold]
          FlatScrollBars = True
          ReadOnly = True
          ParentFont = False
          ShowColumnHeaders = False
          TabOrder = 1
          ViewStyle = vsReport
          OnClick = ListView1Click
          OnCustomDrawSubItem = ListView1CustomDrawSubItem
        end
        object CheckBox1: TCheckBox
          Left = 345
          Top = 23
          Width = 13
          Height = 13
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object ListView6: TListView
          Left = 369
          Top = 430
          Width = 350
          Height = 361
          BorderStyle = bsNone
          Columns = <
            item
              Caption = 'U.'
              Width = 30
            end
            item
              Caption = 'Articulo'
              Width = 295
            end>
          ColumnClick = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -19
          Font.Name = 'Arial Narrow'
          Font.Style = [fsBold]
          FlatScrollBars = True
          ReadOnly = True
          ParentFont = False
          ShowColumnHeaders = False
          TabOrder = 2
          ViewStyle = vsReport
          OnClick = ListView6Click
          OnCustomDrawSubItem = ListView6CustomDrawSubItem
        end
        object ListView7: TListView
          Left = 725
          Top = 430
          Width = 350
          Height = 361
          BorderStyle = bsNone
          Columns = <
            item
              Caption = 'U.'
              Width = 30
            end
            item
              Caption = 'Articulo'
              Width = 295
            end>
          ColumnClick = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -19
          Font.Name = 'Arial Narrow'
          Font.Style = [fsBold]
          FlatScrollBars = True
          ReadOnly = True
          ParentFont = False
          ShowColumnHeaders = False
          TabOrder = 3
          ViewStyle = vsReport
          OnClick = ListView7Click
          OnCustomDrawSubItem = ListView7CustomDrawSubItem
        end
        object ListView8: TListView
          Left = 1081
          Top = 430
          Width = 350
          Height = 361
          BorderStyle = bsNone
          Columns = <
            item
              Caption = 'U.'
              Width = 30
            end
            item
              Caption = 'Articulo'
              Width = 295
            end>
          ColumnClick = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -19
          Font.Name = 'Arial Narrow'
          Font.Style = [fsBold]
          FlatScrollBars = True
          ReadOnly = True
          ParentFont = False
          ShowColumnHeaders = False
          TabOrder = 4
          ViewStyle = vsReport
          OnClick = ListView8Click
          OnCustomDrawSubItem = ListView8CustomDrawSubItem
        end
        object ListView2: TListView
          Left = 369
          Top = 40
          Width = 350
          Height = 361
          BorderStyle = bsNone
          Columns = <
            item
              Caption = 'Unidades'
              Width = 30
            end
            item
              Caption = 'Articulo'
              Width = 295
            end>
          ColumnClick = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -19
          Font.Name = 'Arial Narrow'
          Font.Style = [fsBold]
          FlatScrollBars = True
          ReadOnly = True
          ParentFont = False
          ShowColumnHeaders = False
          TabOrder = 9
          ViewStyle = vsReport
          OnClick = ListView2Click
          OnCustomDrawSubItem = ListView2CustomDrawSubItem
        end
        object ListView3: TListView
          Left = 725
          Top = 40
          Width = 350
          Height = 361
          BorderStyle = bsNone
          Columns = <
            item
              Caption = 'Unidades'
              Width = 30
            end
            item
              Caption = 'Articulo'
              Width = 295
            end>
          ColumnClick = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -19
          Font.Name = 'Arial Narrow'
          Font.Style = [fsBold]
          FlatScrollBars = True
          ReadOnly = True
          ParentFont = False
          ShowColumnHeaders = False
          TabOrder = 10
          ViewStyle = vsReport
          OnClick = ListView3Click
          OnCustomDrawSubItem = ListView3CustomDrawSubItem
        end
        object ListView4: TListView
          Left = 1081
          Top = 40
          Width = 350
          Height = 361
          BorderStyle = bsNone
          Columns = <
            item
              Caption = 'U.'
              Width = 30
            end
            item
              Caption = 'Articulo'
              Width = 295
            end>
          ColumnClick = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -19
          Font.Name = 'Arial Narrow'
          Font.Style = [fsBold]
          FlatScrollBars = True
          ReadOnly = True
          ParentFont = False
          ShowColumnHeaders = False
          TabOrder = 11
          ViewStyle = vsReport
          OnClick = ListView4Click
          OnCustomDrawSubItem = ListView4CustomDrawSubItem
        end
        object ListView5: TListView
          Left = 13
          Top = 430
          Width = 350
          Height = 361
          BorderStyle = bsNone
          Columns = <
            item
              Caption = 'U.'
              Width = 30
            end
            item
              Caption = 'Articulo'
              Width = 295
            end>
          ColumnClick = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -19
          Font.Name = 'Arial Narrow'
          Font.Style = [fsBold]
          FlatScrollBars = True
          ReadOnly = True
          ParentFont = False
          ShowColumnHeaders = False
          TabOrder = 12
          ViewStyle = vsReport
          OnClick = ListView5Click
          OnCustomDrawSubItem = ListView5CustomDrawSubItem
        end
        object CheckBox2: TCheckBox
          Left = 703
          Top = 23
          Width = 13
          Height = 13
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
        end
        object CheckBox3: TCheckBox
          Left = 1058
          Top = 23
          Width = 13
          Height = 13
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 6
        end
        object CheckBox4: TCheckBox
          Left = 1415
          Top = 23
          Width = 13
          Height = 13
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 7
        end
        object CheckBox5: TCheckBox
          Left = 345
          Top = 414
          Width = 13
          Height = 13
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 8
        end
        object CheckBox6: TCheckBox
          Left = 703
          Top = 414
          Width = 13
          Height = 13
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 13
        end
        object CheckBox7: TCheckBox
          Left = 1058
          Top = 414
          Width = 13
          Height = 13
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 14
        end
        object CheckBox8: TCheckBox
          Left = 1415
          Top = 414
          Width = 13
          Height = 13
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 15
        end
      end
      object btnCompletar: TButton
        Left = 1479
        Top = 296
        Width = 114
        Height = 65
        Caption = 'Completar '#13#10' comanda'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        WordWrap = True
        OnClick = btnCompletarClick
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Comandas Completadas'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ImageIndex = 1
      ParentFont = False
      object btnDevolver: TButton
        Left = 1407
        Top = 27
        Width = 114
        Height = 65
        Caption = 'Devolver'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        OnClick = btnDevolverClick
      end
      object GroupBox2: TGroupBox
        Left = 3
        Top = 3
        Width = 1398
        Height = 566
        Caption = 'Completadas'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 1
        object Shape26: TShape
          Left = 32
          Top = 33
          Width = 128
          Height = 128
        end
        object Shape27: TShape
          Left = 200
          Top = 33
          Width = 128
          Height = 128
        end
        object Shape28: TShape
          Left = 365
          Top = 33
          Width = 128
          Height = 128
        end
        object Shape29: TShape
          Left = 532
          Top = 33
          Width = 128
          Height = 128
        end
        object Shape30: TShape
          Left = 696
          Top = 33
          Width = 128
          Height = 128
        end
        object Shape31: TShape
          Left = 32
          Top = 198
          Width = 128
          Height = 128
        end
        object Shape32: TShape
          Left = 200
          Top = 198
          Width = 128
          Height = 128
        end
        object Shape33: TShape
          Left = 365
          Top = 198
          Width = 128
          Height = 128
        end
        object Shape34: TShape
          Left = 532
          Top = 198
          Width = 128
          Height = 128
        end
        object Shape35: TShape
          Left = 696
          Top = 198
          Width = 128
          Height = 128
        end
        object Shape36: TShape
          Left = 32
          Top = 365
          Width = 128
          Height = 128
        end
        object Shape37: TShape
          Left = 200
          Top = 365
          Width = 128
          Height = 128
        end
        object Shape38: TShape
          Left = 365
          Top = 365
          Width = 128
          Height = 128
        end
        object Shape39: TShape
          Left = 532
          Top = 365
          Width = 128
          Height = 128
        end
        object Shape40: TShape
          Left = 696
          Top = 365
          Width = 128
          Height = 128
        end
        object Shape41: TShape
          Left = 32
          Top = 533
          Width = 128
          Height = 128
        end
        object Shape42: TShape
          Left = 200
          Top = 533
          Width = 128
          Height = 128
        end
        object Shape43: TShape
          Left = 365
          Top = 533
          Width = 128
          Height = 128
        end
        object Shape44: TShape
          Left = 532
          Top = 533
          Width = 128
          Height = 128
        end
        object Shape45: TShape
          Left = 696
          Top = 533
          Width = 128
          Height = 128
        end
        object Shape46: TShape
          Left = 32
          Top = 698
          Width = 128
          Height = 128
        end
        object Shape47: TShape
          Left = 200
          Top = 698
          Width = 128
          Height = 128
        end
        object Shape48: TShape
          Left = 365
          Top = 698
          Width = 128
          Height = 128
        end
        object Shape49: TShape
          Left = 532
          Top = 698
          Width = 128
          Height = 128
        end
        object Shape50: TShape
          Left = 696
          Top = 698
          Width = 128
          Height = 128
        end
        object LabelComanda: TLabel
          Left = 872
          Top = 69
          Width = 8
          Height = 33
          Alignment = taCenter
          Caption = ' '
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -27
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          Transparent = False
        end
        object SpeedButton26: TSpeedButton
          Left = 17
          Top = 17
          Width = 56
          Height = 72
          GroupIndex = 1
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = SpeedButton26Click
        end
        object SpeedButton27: TSpeedButton
          Left = 183
          Top = 17
          Width = 160
          Height = 160
          GroupIndex = 1
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = SpeedButton27Click
        end
        object SpeedButton28: TSpeedButton
          Left = 349
          Top = 17
          Width = 160
          Height = 160
          GroupIndex = 1
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = SpeedButton28Click
        end
        object SpeedButton29: TSpeedButton
          Left = 515
          Top = 17
          Width = 160
          Height = 160
          GroupIndex = 1
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = SpeedButton29Click
        end
        object SpeedButton30: TSpeedButton
          Left = 681
          Top = 17
          Width = 160
          Height = 160
          GroupIndex = 1
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = SpeedButton30Click
        end
        object SpeedButton31: TSpeedButton
          Left = 17
          Top = 183
          Width = 160
          Height = 160
          GroupIndex = 1
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = SpeedButton31Click
        end
        object SpeedButton32: TSpeedButton
          Left = 183
          Top = 183
          Width = 160
          Height = 160
          GroupIndex = 1
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = SpeedButton32Click
        end
        object SpeedButton33: TSpeedButton
          Left = 349
          Top = 183
          Width = 160
          Height = 160
          GroupIndex = 1
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = SpeedButton33Click
        end
        object SpeedButton34: TSpeedButton
          Left = 515
          Top = 183
          Width = 160
          Height = 160
          GroupIndex = 1
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = SpeedButton34Click
        end
        object SpeedButton35: TSpeedButton
          Left = 681
          Top = 183
          Width = 160
          Height = 160
          GroupIndex = 1
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = SpeedButton35Click
        end
        object SpeedButton36: TSpeedButton
          Left = 17
          Top = 349
          Width = 160
          Height = 160
          GroupIndex = 1
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = SpeedButton36Click
        end
        object SpeedButton37: TSpeedButton
          Left = 183
          Top = 349
          Width = 160
          Height = 160
          GroupIndex = 1
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = SpeedButton37Click
        end
        object SpeedButton38: TSpeedButton
          Left = 349
          Top = 349
          Width = 160
          Height = 160
          GroupIndex = 1
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = SpeedButton38Click
        end
        object SpeedButton39: TSpeedButton
          Left = 517
          Top = 349
          Width = 160
          Height = 160
          GroupIndex = 1
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = SpeedButton39Click
        end
        object SpeedButton40: TSpeedButton
          Left = 681
          Top = 349
          Width = 160
          Height = 160
          GroupIndex = 1
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = SpeedButton40Click
        end
        object SpeedButton41: TSpeedButton
          Left = 17
          Top = 515
          Width = 160
          Height = 160
          GroupIndex = 1
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = SpeedButton41Click
        end
        object SpeedButton42: TSpeedButton
          Left = 183
          Top = 515
          Width = 160
          Height = 160
          GroupIndex = 1
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = SpeedButton42Click
        end
        object SpeedButton43: TSpeedButton
          Left = 349
          Top = 515
          Width = 160
          Height = 160
          GroupIndex = 1
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = SpeedButton43Click
        end
        object SpeedButton44: TSpeedButton
          Left = 515
          Top = 515
          Width = 160
          Height = 160
          GroupIndex = 1
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = SpeedButton44Click
        end
        object SpeedButton45: TSpeedButton
          Left = 683
          Top = 515
          Width = 160
          Height = 160
          GroupIndex = 1
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = SpeedButton45Click
        end
        object SpeedButton46: TSpeedButton
          Left = 17
          Top = 681
          Width = 160
          Height = 160
          GroupIndex = 1
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = SpeedButton46Click
        end
        object SpeedButton47: TSpeedButton
          Left = 183
          Top = 681
          Width = 160
          Height = 160
          GroupIndex = 1
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = SpeedButton47Click
        end
        object SpeedButton48: TSpeedButton
          Left = 349
          Top = 681
          Width = 160
          Height = 160
          GroupIndex = 1
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = SpeedButton48Click
        end
        object SpeedButton49: TSpeedButton
          Left = 515
          Top = 681
          Width = 160
          Height = 160
          GroupIndex = 1
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = SpeedButton49Click
        end
        object SpeedButton50: TSpeedButton
          Left = 681
          Top = 681
          Width = 160
          Height = 160
          GroupIndex = 1
          Flat = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWhite
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          OnClick = SpeedButton50Click
        end
        object Shape1: TShape
          Left = 967
          Top = 288
          Width = 9
          Height = 521
          Brush.Color = clBlack
          Shape = stRoundRect
        end
        object ListaComanda: TListView
          Left = 872
          Top = 104
          Width = 505
          Height = 739
          Columns = <
            item
              Caption = 'Unidades'
              Width = 30
            end
            item
              Caption = 'Articulo'
              Width = 470
            end>
          Font.Charset = ANSI_CHARSET
          Font.Color = clWhite
          Font.Height = -21
          Font.Name = 'Arial Narrow'
          Font.Style = [fsBold]
          ParentFont = False
          ShowColumnHeaders = False
          TabOrder = 0
          ViewStyle = vsReport
          Visible = False
          OnCustomDrawSubItem = ListaComandaCustomDrawSubItem
        end
        object GroupBox3: TGroupBox
          Left = 830
          Top = 172
          Width = 507
          Height = 72
          Caption = 'Fecha'
          TabOrder = 1
          object Label11: TLabel
            Left = 227
            Top = 53
            Width = 10
            Height = 16
            Caption = 'al'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label12: TLabel
            Left = 26
            Top = 33
            Width = 17
            Height = 16
            Caption = 'del'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object dtpFfin: TDateTimePicker
            Left = 286
            Top = 26
            Width = 19
            Height = 24
            Date = 2958101.670990174000000000
            Format = 'dd/MM/yyyy'
            Time = 2958101.670990174000000000
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            OnChange = dtpFfinChange
          end
          object dtpFini: TDateTimePicker
            Left = 50
            Top = 26
            Width = 23
            Height = 24
            Date = 43969.670983796300000000
            Format = 'dd/MM/yyyy'
            Time = 43969.670983796300000000
            DoubleBuffered = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentDoubleBuffered = False
            ParentFont = False
            TabOrder = 1
            OnChange = dtpFiniChange
          end
          object dtpTFin: TDateTimePicker
            Left = 397
            Top = 26
            Width = 93
            Height = 24
            Date = 43969.708940335650000000
            Time = 43969.708940335650000000
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Kind = dtkTime
            ParentFont = False
            TabOrder = 2
            OnChange = dtpTFinChange
          end
          object dtpTIni: TDateTimePicker
            Left = 166
            Top = 26
            Width = 93
            Height = 24
            Date = 43969.708940335650000000
            Time = 43969.708940335650000000
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Kind = dtkTime
            ParentFont = False
            TabOrder = 3
            OnChange = dtpTIniChange
          end
        end
      end
      object btnPreferencias: TButton
        Left = 1407
        Top = 98
        Width = 114
        Height = 66
        Caption = 'Preferencias'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -17
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        OnClick = btnPreferenciasClick
      end
      object btnContraseña: TButton
        Left = 1407
        Top = 335
        Width = 107
        Height = 73
        Caption = 'Cambiar'#13#10'Contrase'#241'a'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -17
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
        WordWrap = True
        OnClick = btnContraseñaClick
      end
    end
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'DriverID=FB'
      'User_Name=sysdba'
      'Password=masterkey')
    Left = 248
    Top = 8
  end
  object Timer_comandas_pend: TTimer
    Interval = 10000
    OnTimer = Timer_comandas_pendTimer
    Left = 272
    Top = 8
  end
  object qryComandasPend: TFDQuery
    Connection = Conexion
    Left = 376
    Top = 8
  end
  object Timer_comandas_comp: TTimer
    Interval = 10000
    OnTimer = Timer_comandas_compTimer
    Left = 512
    Top = 16
  end
  object qryComandasComp: TFDQuery
    Connection = Conexion
    Left = 464
    Top = 24
  end
  object qryComandasPendDet: TFDQuery
    Connection = Conexion
    Left = 496
    Top = 48
  end
  object qryFiltro: TFDQuery
    Connection = Conexion
    Left = 607
    Top = 40
  end
  object qryComandas: TFDQuery
    Connection = Conexion
    Left = 503
    Top = 203
  end
end
