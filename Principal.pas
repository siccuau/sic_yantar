unit Principal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client,
  Vcl.ComCtrls, Vcl.CategoryButtons, Vcl.ButtonGroup, ExtCtrls,
  Vcl.StdCtrls, Vcl.Buttons, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet, Vcl.DBCtrls,UContrasena;

type
//TShape = class(ExtCtrls.TShape); //interposer class

  TUPrincipal = class(TForm)
    StatusBar1: TStatusBar;
    Conexion: TFDConnection;
    GroupBox1: TGroupBox;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Timer_comandas_pend: TTimer;
    qryComandasPend: TFDQuery;
    btnCompletar: TButton;
    TabSheet2: TTabSheet;
    btnDevolver: TButton;
    GroupBox2: TGroupBox;
    SpeedButton26: TSpeedButton;
    SpeedButton27: TSpeedButton;
    SpeedButton28: TSpeedButton;
    SpeedButton29: TSpeedButton;
    SpeedButton30: TSpeedButton;
    SpeedButton31: TSpeedButton;
    SpeedButton32: TSpeedButton;
    SpeedButton33: TSpeedButton;
    SpeedButton34: TSpeedButton;
    SpeedButton35: TSpeedButton;
    SpeedButton36: TSpeedButton;
    SpeedButton37: TSpeedButton;
    SpeedButton38: TSpeedButton;
    SpeedButton39: TSpeedButton;
    SpeedButton40: TSpeedButton;
    SpeedButton41: TSpeedButton;
    SpeedButton42: TSpeedButton;
    SpeedButton43: TSpeedButton;
    SpeedButton44: TSpeedButton;
    SpeedButton45: TSpeedButton;
    SpeedButton46: TSpeedButton;
    SpeedButton47: TSpeedButton;
    SpeedButton48: TSpeedButton;
    SpeedButton49: TSpeedButton;
    SpeedButton50: TSpeedButton;
    Timer_comandas_comp: TTimer;
    qryComandasComp: TFDQuery;
    qryComandasPendDet: TFDQuery;
    CheckBox1: TCheckBox;
    ListView1: TListView;
    ListView2: TListView;
    CheckBox2: TCheckBox;
    ListView3: TListView;
    CheckBox3: TCheckBox;
    ListView4: TListView;
    CheckBox4: TCheckBox;
    ListView5: TListView;
    CheckBox5: TCheckBox;
    ListView6: TListView;
    ListView7: TListView;
    ListView8: TListView;
    CheckBox6: TCheckBox;
    CheckBox7: TCheckBox;
    CheckBox8: TCheckBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    btnPreferencias: TButton;
    qryFiltro: TFDQuery;
    Label9: TLabel;
    Label10: TLabel;
    lblComedor: TLabel;
    lblDomicilio: TLabel;
    btnContraseña: TButton;
    Shape26: TShape;
    Shape27: TShape;
    Shape28: TShape;
    Shape29: TShape;
    Shape30: TShape;
    Shape31: TShape;
    Shape32: TShape;
    Shape33: TShape;
    Shape34: TShape;
    Shape35: TShape;
    Shape36: TShape;
    Shape37: TShape;
    Shape38: TShape;
    Shape39: TShape;
    Shape40: TShape;
    Shape41: TShape;
    Shape42: TShape;
    Shape43: TShape;
    Shape44: TShape;
    Shape45: TShape;
    Shape46: TShape;
    Shape47: TShape;
    Shape48: TShape;
    Shape49: TShape;
    Shape50: TShape;
    ListaComanda: TListView;
    LabelComanda: TLabel;
    qryComandas: TFDQuery;
    Shape1: TShape;
    dtpFini: TDateTimePicker;
    Label11: TLabel;
    dtpFfin: TDateTimePicker;
    Label12: TLabel;
    dtpTIni: TDateTimePicker;
    dtpTFin: TDateTimePicker;
    Texto1: TLabel;
    Texto2: TLabel;
    Texto3: TLabel;
    Texto4: TLabel;
    Texto5: TLabel;
    Texto6: TLabel;
    Texto7: TLabel;
    Texto8: TLabel;
    GroupBox3: TGroupBox;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure Timer_comandas_pendTimer(Sender: TObject);
    procedure ActualizarComandasPend();
     procedure ActualizarComandasComp();
     procedure LimpiarListas();
     procedure ActualizarLista(nombre:String;color:TBrush);
    procedure btnCompletarClick(Sender: TObject);
    procedure Timer_comandas_compTimer(Sender: TObject);
    procedure btnDevolverClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure SpeedButton1MouseEnter(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2MouseEnter(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure Label1Click(Sender: TObject);
    procedure ListView1Click(Sender: TObject);
    procedure ListView2Click(Sender: TObject);
    procedure ListView3Click(Sender: TObject);
    procedure ListView4Click(Sender: TObject);
    procedure ListView5Click(Sender: TObject);
    procedure ListView6Click(Sender: TObject);
    procedure ListView7Click(Sender: TObject);
    procedure ListView8Click(Sender: TObject);
    procedure btnPreferenciasClick(Sender: TObject);
    procedure btnContraseñaClick(Sender: TObject);
    procedure SpeedButton26Click(Sender: TObject);
    procedure SpeedButton27Click(Sender: TObject);
    procedure SpeedButton28Click(Sender: TObject);
    procedure SpeedButton29Click(Sender: TObject);
    procedure SpeedButton30Click(Sender: TObject);
    procedure SpeedButton31Click(Sender: TObject);
    procedure SpeedButton32Click(Sender: TObject);
    procedure SpeedButton33Click(Sender: TObject);
    procedure SpeedButton34Click(Sender: TObject);
    procedure SpeedButton35Click(Sender: TObject);
    procedure SpeedButton36Click(Sender: TObject);
    procedure SpeedButton37Click(Sender: TObject);
    procedure SpeedButton39Click(Sender: TObject);
    procedure SpeedButton38Click(Sender: TObject);
    procedure SpeedButton40Click(Sender: TObject);
    procedure SpeedButton41Click(Sender: TObject);
    procedure SpeedButton42Click(Sender: TObject);
    procedure SpeedButton43Click(Sender: TObject);
    procedure SpeedButton44Click(Sender: TObject);
    procedure SpeedButton45Click(Sender: TObject);
    procedure SpeedButton46Click(Sender: TObject);
    procedure SpeedButton47Click(Sender: TObject);
    procedure SpeedButton48Click(Sender: TObject);
    procedure SpeedButton49Click(Sender: TObject);
    procedure SpeedButton50Click(Sender: TObject);
    procedure dtpFiniChange(Sender: TObject);
    procedure dtpFfinChange(Sender: TObject);
    procedure dtpTIniChange(Sender: TObject);
    procedure dtpTFinChange(Sender: TObject);
    procedure ListView1CustomDrawSubItem(Sender: TCustomListView;
      Item: TListItem; SubItem: Integer; State: TCustomDrawState;
      var DefaultDraw: Boolean);
    procedure ListView2CustomDrawSubItem(Sender: TCustomListView;
      Item: TListItem; SubItem: Integer; State: TCustomDrawState;
      var DefaultDraw: Boolean);
    procedure ListView3CustomDrawSubItem(Sender: TCustomListView;
      Item: TListItem; SubItem: Integer; State: TCustomDrawState;
      var DefaultDraw: Boolean);
    procedure ListView4CustomDrawSubItem(Sender: TCustomListView;
      Item: TListItem; SubItem: Integer; State: TCustomDrawState;
      var DefaultDraw: Boolean);
    procedure ListView5CustomDrawSubItem(Sender: TCustomListView;
      Item: TListItem; SubItem: Integer; State: TCustomDrawState;
      var DefaultDraw: Boolean);
    procedure ListView6CustomDrawSubItem(Sender: TCustomListView;
      Item: TListItem; SubItem: Integer; State: TCustomDrawState;
      var DefaultDraw: Boolean);
    procedure ListView7CustomDrawSubItem(Sender: TCustomListView;
      Item: TListItem; SubItem: Integer; State: TCustomDrawState;
      var DefaultDraw: Boolean);
    procedure ListView8CustomDrawSubItem(Sender: TCustomListView;
      Item: TListItem; SubItem: Integer; State: TCustomDrawState;
      var DefaultDraw: Boolean);
    procedure ListaComandaCustomDrawSubItem(Sender: TCustomListView;
      Item: TListItem; SubItem: Integer; State: TCustomDrawState;
      var DefaultDraw: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  UPrincipal: TUPrincipal;

implementation

{$R *.dfm}


procedure TUPrincipal.btnCompletarClick(Sender: TObject);
var
i:integer;
check:TCheckBox;
titulo:TLabel;
begin
 //RECORRER CHECK BOX
for i := 1 to 8 do
  begin
  check:=FindComponent('CheckBox'+inttostr(i)) as TCheckBox;
  titulo:=FindComponent('Texto'+inttostr(i)) as TLabel;
  if check.Checked then
    begin
    if titulo.Caption<>'' then
      begin
      Conexion.ExecSQL('update MGTOUCH_VENTA_COMEDOR set estado=''C'' where clave_cliente=:cl',[titulo.Caption]);
      Conexion.Commit;
      end;
    end;
  end;
LimpiarListas();
ActualizarComandasPend();
end;

procedure TUPrincipal.btnContraseñaClick(Sender: TObject);
var
value:string;
contr : TContrasena;
begin
if Conexion.ExecSQLScalar('select count(CONTRASENA) from MGTOUCH_SIC_CONTRASENA')=0 then
   begin
    repeat
    value := inputbox('Contraseña', 'Asignar contraseña nueva: ', '');
     until value <> '';
    Conexion.ExecSQL('insert into MGTOUCH_SIC_CONTRASENA(contrasena) values(:v)',[value]);
    Conexion.Commit;
   end
   else
   begin
   contr := TContrasena.Create(nil);
   contr.Conexion.Params := Conexion.Params;
   contr.Conexion.Open;
   contr.nombre_boton:='Cambiar Contrasena';
   contr.ShowModal;
   end;
end;

procedure TUPrincipal.btnDevolverClick(Sender: TObject);
var
i:integer;
boton:TSpeedButton;
value:string;
  contr : TContrasena;
  figura:TShape;
begin

if Conexion.ExecSQLScalar('select count(CONTRASENA) from MGTOUCH_SIC_CONTRASENA')=0 then
   begin
    repeat
    value := inputbox('Contraseña', 'Asignar contraseña nueva: ', '');
     until value <> '';
    Conexion.ExecSQL('insert into MGTOUCH_SIC_CONTRASENA(contrasena) values(:v)',[value]);
    Conexion.Commit;
   end
   else
   begin
   contr := TContrasena.Create(nil);
   contr.Conexion.Params := Conexion.Params;
   contr.Conexion.Open;
   contr.nombre_boton:='Devolver';

   if contr.ShowModal=mrOk then
    begin
     //RECORRER BOTONES
    for i := 26 to 50 do
      begin
      boton:=FindComponent('SpeedButton'+inttostr(i)) as TSpeedButton;
      figura:= FindComponent('Shape'+inttostr(i)) as TShape;
      if boton.Down then
        begin
        if boton.Caption<>'' then
          begin
          Conexion.ExecSQL('update MGTOUCH_VENTA_COMEDOR set estado=''N'' where clave_cliente=:cl',[boton.Caption]);
          Conexion.Commit;
          end;
        end;
      end;
    ActualizarComandasComp();
    ActualizarComandasPend();

        for i := 26 to 50 do
      begin
      boton:=FindComponent('SpeedButton'+inttostr(i)) as TSpeedButton;
      if boton.Down then
        begin
        boton.Click;
        end;
      end;
    end
    else ShowMessage('Contraseña Incorrecta.');
   end;
end;

procedure TUPrincipal.btnPreferenciasClick(Sender: TObject);
var
  contr : TContrasena;
  value : string;
begin
if Conexion.ExecSQLScalar('select count(CONTRASENA) from MGTOUCH_SIC_CONTRASENA')=0 then
   begin
    repeat
    value := inputbox('Contraseña', 'Asignar contraseña nueva: ', '');
     until value <> '';
    Conexion.ExecSQL('insert into MGTOUCH_SIC_CONTRASENA(contrasena) values(:v)',[value]);
    Conexion.Commit;
   end
   else
   begin
    contr := TContrasena.Create(nil);
    contr.Conexion.Params := Conexion.Params;
    contr.Conexion.Open;
    contr.nombre_boton:='Preferencias';
    contr.ShowModal;
   end;
end;

procedure TUPrincipal.dtpFfinChange(Sender: TObject);
begin
ActualizarComandasComp();
end;

procedure TUPrincipal.dtpFiniChange(Sender: TObject);
begin
ActualizarComandasComp();
end;

procedure TUPrincipal.dtpTFinChange(Sender: TObject);
begin
ActualizarComandasComp();
end;

procedure TUPrincipal.dtpTIniChange(Sender: TObject);
begin
ActualizarComandasComp();
end;

procedure TUPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
Application.Terminate;
end;

procedure TUPrincipal.FormShow(Sender: TObject);
var
tabla:integer;
  Col: TListColumn;
  i,x,separacion,y: Integer;
  comanda:TListView;
begin
tabla := Conexion.ExecSQLScalar('select count(*) from rdb$relations where rdb$relation_name = ''MGTOUCH_VENTA_COMEDOR'' ');
  if tabla = 0  then
  begin
    Conexion.ExecSQL(' CREATE TABLE MGTOUCH_VENTA_COMEDOR ('+
   ' CLIENTE_ID           INTEGER,'+
   ' CLAVE_CLIENTE        VARCHAR(20),'+
   ' VENDEDOR_ID          INTEGER,'+
   ' TICKET_IMPRESO       CHAR(1) DEFAULT ''N'','+
   ' TIPO                 CHAR(1) DEFAULT ''N'' NOT NULL,'+
   ' NUM_PERSONAS         INTEGER,'+
   ' SUB_CLIENTE_ID       INTEGER,'+
   ' NINOS                INTEGER,'+
   ' ADULTOS              INTEGER,'+
   ' FECHA_HORA_CREACION  TIMESTAMP DEFAULT ''NOW'','+
   ' USUARIO_CREADOR      VARCHAR(31) DEFAULT user,'+
   ' DSCTO1               NUMERIC(9,6) DEFAULT 0,'+
   ' OTRO_CLIENTE_ID      INTEGER,'+
   ' REFERENCIA           INTEGER,'+
   ' DIVIDIR_CUENTA       INTEGER,'+
   ' TIPO_DSCTO           CHAR(1),'+
   ' DSCTO_IMPORTE        NUMERIC(15,2),'+
   ' DESCRIPCION          VARCHAR(200),'+
   ' TIPO_CLIENTE_ID      INTEGER,'+
   ' ESTADO               CHAR(1) DEFAULT ''N'''+
    ' );');
    Conexion.ExecSQL('grant all on MGTOUCH_VENTA_COMEDOR to usuario_microsip');
  end
  else
  begin
   Conexion.ExecSQL('EXECUTE block as'+
    ' BEGIN'+
    ' if (not exists('+
    ' select 1 from RDB$RELATION_FIELDS rf '+
    ' where rf.RDB$RELATION_NAME = ''MGTOUCH_VENTA_COMEDOR'' and rf.RDB$FIELD_NAME = ''ESTADO''))'+
    ' then'+
    ' execute statement ''ALTER TABLE MGTOUCH_VENTA_COMEDOR ADD ESTADO CHAR(1) DEFAULT ''''N'''''';'+
    ' END');
  end;

  tabla := Conexion.ExecSQLScalar('select count(*) from rdb$relations where rdb$relation_name = ''MGTOUCH_SIC_LINEAS_ART'' ');
  if tabla = 0  then
  begin
    Conexion.ExecSQL(' CREATE TABLE MGTOUCH_SIC_LINEAS_ART ('+
   ' LINEA_ARTICULO_ID           INTEGER'+
    ' );');
    Conexion.ExecSQL('grant all on MGTOUCH_SIC_LINEAS_ART to usuario_microsip');
  end;
    tabla := Conexion.ExecSQLScalar('select count(*) from rdb$relations where rdb$relation_name = ''MGTOUCH_SIC_PREFERENCIAS'' ');
  if tabla = 0  then
  begin
    Conexion.ExecSQL(' CREATE TABLE MGTOUCH_SIC_PREFERENCIAS ('+
   ' FILTRO_LINEAS_ART           CHAR(1) DEFAULT ''N'''+
    ' );');
    Conexion.ExecSQL('grant all on MGTOUCH_SIC_PREFERENCIAS to usuario_microsip');
    Conexion.ExecSQL('insert into MGTOUCH_SIC_PREFERENCIAS(filtro_lineas_art) values(''N'')') ;
    Conexion.Commit;
  end;

      tabla := Conexion.ExecSQLScalar('select count(*) from rdb$relations where rdb$relation_name = ''MGTOUCH_SIC_CONTRASENA'' ');
  if tabla = 0  then
  begin
    Conexion.ExecSQL(' CREATE TABLE MGTOUCH_SIC_CONTRASENA ('+
   ' CONTRASENA           VARCHAR(50)'+
    ' );');
    Conexion.ExecSQL('grant all on MGTOUCH_SIC_CONTRASENA to usuario_microsip');
    Conexion.Commit;
  end;

  PageControl1.ActivePageIndex:=0;
  ActualizarComandasPend();
  ActualizarComandasComp();

  lblComedor.Color:=clWebDarkgreen;
  lblDomicilio.Color:=clWebRoyalBlue;

  dtpFini.Date:=now;

SetBounds(Left, Top, Screen.Width, Screen.Height);

/////////////////////////////////////////////COMANDAS PENDIENTES///////////////////////////////////////////////////////////
x:=0;
y:=Label1.Height+5;
separacion:=10;

//GROUPBOX
  GroupBox1.Width:=Screen.Width-btnCompletar.Width-20;
  GroupBox1.Height:=Screen.Height-lblComedor.Height-80;

//LABELS 1-4
for i := 1 to 4 do
 begin
 TLabel(FindComponent('Label'+inttostr(i))).Width:=Round((GroupBox1.Width/4)-separacion);
 TLabel(FindComponent('Label'+inttostr(i))).Left:=x+separacion;
 x:=x+TLabel(FindComponent('Label'+inttostr(i))).Width+separacion;
 end;

 x:=0;
 //LABELS 4-8
for i := 5 to 8 do
 begin
 TLabel(FindComponent('Label'+inttostr(i))).Width:=Round((GroupBox1.Width/4)-separacion);
 TLabel(FindComponent('Label'+inttostr(i))).Left:=x+separacion;
 x:=x+TLabel(FindComponent('Label'+inttostr(i))).Width+separacion;
   TLabel(FindComponent('Label'+inttostr(i))).Top:=TListView(FindComponent('ListView'+inttostr(i-4))).Height+(TLabel(FindComponent('Label'+inttostr(i-4))).Height)+30;
 end;

 x:=0;
//LIST VIEW 1-4
for i := 1 to 4 do
 begin
 TListView(FindComponent('ListView'+inttostr(i))).Width:=TLabel(FindComponent('Label'+inttostr(i))).Width;
  TListView(FindComponent('ListView'+inttostr(i))).Height:=Round((GroupBox1.Height/2)-separacion-TLabel(FindComponent('Label'+inttostr(i))).Height);
 TListView(FindComponent('ListView'+inttostr(i))).Left:=x+separacion;
  TListView(FindComponent('ListView'+inttostr(i))).Top:=y+separacion;
 x:=x+TListView(FindComponent('ListView'+inttostr(i))).Width+separacion;
 end;
  x:=0;
  //LIST VIEW 4-8
 for i := 5 to 8 do
 begin
 TListView(FindComponent('ListView'+inttostr(i))).Width:=Round((GroupBox1.Width/4)-separacion);
   TListView(FindComponent('ListView'+inttostr(i))).Height:=TListView(FindComponent('ListView'+inttostr(i-4))).Height;
 TListView(FindComponent('ListView'+inttostr(i))).Left:=x+separacion;
   TListView(FindComponent('ListView'+inttostr(i))).Top:=TListView(FindComponent('ListView'+inttostr(i-4))).Height+(TLabel(FindComponent('Label'+inttostr(i-4))).Height*2)+27;
 x:=x+TListView(FindComponent('ListView'+inttostr(i))).Width+separacion;
 end;

 //CHECKBOX 1-8
for i := 1 to 8 do
 begin
 TCheckBox(FindComponent('CheckBox'+inttostr(i))).Left:=TLabel(FindComponent('Label'+inttostr(i))).Left+10;
 TCheckBox(FindComponent('CheckBox'+inttostr(i))).Top:=TLabel(FindComponent('Label'+inttostr(i))).Top+5;
 end;

 //LABELS INDICADORES
 label9.Top:=Screen.Height-label9.Height-70;
 label9.Left:=15;
 lblComedor.Top:=Screen.Height-lblComedor.Height-70;
 lblComedor.Left:=label9.Width+20;
 label10.Top:=Screen.Height-label10.Height-70;
 label10.Left:=label9.Width+lblComedor.Width+50;
 lblDomicilio.Top:=Screen.Height-lblDomicilio.Height-70;
 lblDomicilio.Left:=label9.Width+lblComedor.Width+label10.Width+55;

 //BOTON COMPLETAR
 btnCompletar.Top:=label4.Top;
 btnCompletar.Left:=Screen.Width-btnCompletar.Width-10;
 /////////////////////////////////////////////////////////////////////////////////////////////////////////////
 ///
 ///////////////////////////////////  COMANDAS COMPLETAS ////////////////////////////////////////////////////
//GROUPBOX
  GroupBox2.Width:=Screen.Width-btnDevolver.Width-20;
  GroupBox2.Height:=Screen.Height-60;

  //BOTONES
 btnDevolver.Top:=SpeedButton26.Top;
 btnDevolver.Left:=Screen.Width-btnDevolver.Width-10;

 btnPreferencias.Top:=btnDevolver.Top+btnPreferencias.Height+5;
 btnPreferencias.Left:=btnDevolver.Left;

 btnContraseña.Top:=btnPreferencias.Top+btnContraseña.Height+5;
 btnContraseña.Left:=btnDevolver.Left;

 x:=0;
 //SPEEDBUTTONS 26-30
for i := 26 to 30 do
 begin
 TSpeedButton(FindComponent('SpeedButton'+inttostr(i))).Width:=Round(((Screen.Width/2)/5)-5);
 TSpeedButton(FindComponent('SpeedButton'+inttostr(i))).Height:=Round((groupBox2.Height/5)-10);
 TSpeedButton(FindComponent('SpeedButton'+inttostr(i))).Left:=x+20;
 x:=x+TSpeedButton(FindComponent('SpeedButton'+inttostr(i))).Width+5;
 end;


  //SPEEDBUTTONS 31-50
for i := 31 to 50 do
 begin
 TSpeedButton(FindComponent('SpeedButton'+inttostr(i))).Width:=Round(((Screen.Width/2)/5)-5);
 TSpeedButton(FindComponent('SpeedButton'+inttostr(i))).Height:=TSpeedButton(FindComponent('SpeedButton'+inttostr(i-5))).Height;
 TSpeedButton(FindComponent('SpeedButton'+inttostr(i))).Left:=TSpeedButton(FindComponent('SpeedButton'+inttostr(i-5))).Left;
 TSpeedButton(FindComponent('SpeedButton'+inttostr(i))).Top:=TSpeedButton(FindComponent('SpeedButton'+inttostr(i-5))).Top+TSpeedButton(FindComponent('SpeedButton'+inttostr(i-5))).Height+5;
 end;

 x:=0;
  //SHAPES 26-30
for i := 26 to 30 do
 begin
 TShape(FindComponent('Shape'+inttostr(i))).Width:=Round(((Screen.Width/2)/5)-45);
 TShape(FindComponent('Shape'+inttostr(i))).Height:=Round((groupBox2.Height/5)-50);
 TShape(FindComponent('Shape'+inttostr(i))).Left:=x+40;
  TShape(FindComponent('Shape'+inttostr(i))).Top:=TSpeedButton(FindComponent('SpeedButton'+inttostr(i))).Top+20;
 x:=x+TShape(FindComponent('Shape'+inttostr(i))).Width+45;
 end;

   //SHAPES 31-50
for i := 31 to 50 do
 begin
 TShape(FindComponent('Shape'+inttostr(i))).Width:=TShape(FindComponent('Shape'+inttostr(i-5))).Width;
 TShape(FindComponent('Shape'+inttostr(i))).Height:=TShape(FindComponent('Shape'+inttostr(i-5))).Height;
 TShape(FindComponent('Shape'+inttostr(i))).Left:=TSpeedButton(FindComponent('Shape'+inttostr(i-5))).Left;
 TShape(FindComponent('Shape'+inttostr(i))).Top:=TSpeedButton(FindComponent('Shape'+inttostr(i-5))).Top+TSpeedButton(FindComponent('Shape'+inttostr(i-5))).Height+45;
 end;

 //SHAPE 1 BARRA SEPARADORA
 TShape(FindComponent('Shape1')).Height:=GroupBox2.Height-30;
 TShape(FindComponent('Shape1')).Left:=TSpeedButton(FindComponent('SpeedButton30')).Left+TSpeedButton(FindComponent('SpeedButton30')).Width+20;
 TShape(FindComponent('Shape1')).Top:=GroupBox2.Top+15;

 //GROUPBOX 3 FECHA
 groupBox3.Top:=SpeedButton30.Top;
 groupBox3.Left:=Shape1.Left+Shape1.Width+20;
 groupbox3.Width:=groupbox2.Width-(SpeedButton26.Width*5)-shape1.Width-100;

 //LABELCOMANDA
 labelcomanda.Left:=groupbox3.Left;
 labelcomanda.Top:=groupbox3.Top+GroupBox3.Height+10;
  labelcomanda.Width:=groupbox3.Width;

 //LISTA COMANDA
 ListaComanda.Height:=groupbox2.Height-LabelComanda.Height-GroupBox3.Height-50;
 ListaComanda.Left:=labelcomanda.Left;
 ListaComanda.Top:=labelcomanda.Top+labelcomanda.Height;
 ListaComanda.Width:=groupbox3.Width;

 //DTP DENTOR DE GROUPBOX 3

  dtpFini.Width:=Round((groupbox3.Width-label11.Width-Label12.Width-50)/4);
  dtpTIni.Width:=dtpFini.Width;
  dtpFfin.Width:=dtpFini.Width;
  dtpTFin.Width:=dtpFini.Width;

 label12.Left:=20;
 dtpFini.Left:=label12.Left+label12.Width+5;
 dtpTini.Left:=dtpFini.Left+dtpFini.Width+5;
 label11.Left:=dtpTini.Left+dtpTini.Width+5;
 dtpFfin.Left:=label11.Left+label11.Width+5;
 dtpTFin.Left:=dtpFfin.Left+dtpFfin.Width+5;

  dtpFini.Top:=label12.Top-5;
 dtpTini.Top:=label12.Top-5;
 label11.Top:=label12.Top;
 dtpFfin.Top:=label12.Top-5;
 dtpTFin.Top:=label12.Top-5;

end;

procedure TUPrincipal.Label1Click(Sender: TObject);
begin
CheckBox1.Checked;
end;



procedure TUPrincipal.ListaComandaCustomDrawSubItem(Sender: TCustomListView;
  Item: TListItem; SubItem: Integer; State: TCustomDrawState;
  var DefaultDraw: Boolean);
  var
  s:string;
begin
  //Check if the value of the third column is negative,
  //if so change it's font color to Red (clRed).
  try
  s:=Item.SubItems[0];
  if s.Length>0 then
  if s[1]='(' then
    begin
    Sender.Canvas.Font.Color := clYellow;
    Sender.Canvas.Font.Size:=15;
    end;
  except
    on EConvertError do
      next;
  end;
end;

procedure TUPrincipal.ListView1Click(Sender: TObject);
begin
if CheckBox1.Checked then
  begin

  end
  else
  begin
  CheckBox1.Checked:=true;
  CheckBox1.Visible:=true;
  CheckBox2.Checked:=false;
  CheckBox3.Checked:=false;
  CheckBox4.Checked:=false;
  CheckBox5.Checked:=false;
  CheckBox6.Checked:=false;
  CheckBox7.Checked:=false;
  CheckBox8.Checked:=false;
  ActualizarComandasPend;

  end;
end;

procedure TUPrincipal.ListView1CustomDrawSubItem(Sender: TCustomListView;
  Item: TListItem; SubItem: Integer; State: TCustomDrawState;
  var DefaultDraw: Boolean);
  var
  s:string;
begin
  //Check if the value of the third column is negative,
  //if so change it's font color to Red (clRed).
  try
  s:=Item.SubItems[0];
  if s.Length>0 then
  if s[1]='(' then
    begin
    Sender.Canvas.Font.Color := clYellow;
    Sender.Canvas.Font.Size:=15;
    end;
  except
    on EConvertError do
      next;
  end;
end;

procedure TUPrincipal.ListView2Click(Sender: TObject);
begin
if CheckBox2.Checked then
  begin

  end
  else
  begin
  CheckBox2.Checked:=true;
  CheckBox2.Visible:=true;
  CheckBox1.Checked:=false;
  CheckBox3.Checked:=false;
  CheckBox4.Checked:=false;
  CheckBox5.Checked:=false;
  CheckBox6.Checked:=false;
  CheckBox7.Checked:=false;
  CheckBox8.Checked:=false;
  ActualizarComandasPend;

  end;
end;

procedure TUPrincipal.ListView2CustomDrawSubItem(Sender: TCustomListView;
  Item: TListItem; SubItem: Integer; State: TCustomDrawState;
  var DefaultDraw: Boolean);
  var
  s:string;
begin
  //Check if the value of the third column is negative,
  //if so change it's font color to Red (clRed).
  try
  s:=Item.SubItems[0];
  if s.Length>0 then
  if s[1]='(' then
    begin
    Sender.Canvas.Font.Color := clYellow;
    Sender.Canvas.Font.Size:=15;
    end;
  except
    on EConvertError do
      next;
  end;
end;

procedure TUPrincipal.ListView3Click(Sender: TObject);
begin
if CheckBox3.Checked then
  begin

  end
  else
  begin
  CheckBox3.Checked:=true;
  CheckBox3.Visible:=true;
  CheckBox1.Checked:=false;
  CheckBox2.Checked:=false;
  CheckBox4.Checked:=false;
  CheckBox5.Checked:=false;
  CheckBox6.Checked:=false;
  CheckBox7.Checked:=false;
  CheckBox8.Checked:=false;
  ActualizarComandasPend;

  end;
end;

procedure TUPrincipal.ListView3CustomDrawSubItem(Sender: TCustomListView;
  Item: TListItem; SubItem: Integer; State: TCustomDrawState;
  var DefaultDraw: Boolean);
  var
  s:string;
begin
  //Check if the value of the third column is negative,
  //if so change it's font color to Red (clRed).
  try
  s:=Item.SubItems[0];
  if s.Length>0 then
  if s[1]='(' then
    begin
    Sender.Canvas.Font.Color := clYellow;
    Sender.Canvas.Font.Size:=15;
    end;
  except
    on EConvertError do
      next;
  end;
end;

procedure TUPrincipal.ListView4Click(Sender: TObject);
begin
if CheckBox4.Checked then
  begin

  end
  else
  begin
  CheckBox4.Checked:=true;
  CheckBox4.Visible:=true;
  CheckBox2.Checked:=false;
  CheckBox3.Checked:=false;
  CheckBox1.Checked:=false;
  CheckBox5.Checked:=false;
  CheckBox6.Checked:=false;
  CheckBox7.Checked:=false;
  CheckBox8.Checked:=false;
  ActualizarComandasPend;

  end;
end;

procedure TUPrincipal.ListView4CustomDrawSubItem(Sender: TCustomListView;
  Item: TListItem; SubItem: Integer; State: TCustomDrawState;
  var DefaultDraw: Boolean);
  var
  s:string;
begin
  //Check if the value of the third column is negative,
  //if so change it's font color to Red (clRed).
  try
  s:=Item.SubItems[0];
  if s.Length>0 then
  if s[1]='(' then
    begin
    Sender.Canvas.Font.Color := clYellow;
    Sender.Canvas.Font.Size:=15;
    end;
  except
    on EConvertError do
      next;
  end;
end;

procedure TUPrincipal.ListView5Click(Sender: TObject);
begin
if CheckBox5.Checked then
  begin

  end
  else
  begin
  CheckBox5.Checked:=true;
  CheckBox5.Visible:=true;
  CheckBox2.Checked:=false;
  CheckBox3.Checked:=false;
  CheckBox4.Checked:=false;
  CheckBox1.Checked:=false;
  CheckBox6.Checked:=false;
  CheckBox7.Checked:=false;
  CheckBox8.Checked:=false;
  ActualizarComandasPend;

  end;
end;

procedure TUPrincipal.ListView5CustomDrawSubItem(Sender: TCustomListView;
  Item: TListItem; SubItem: Integer; State: TCustomDrawState;
  var DefaultDraw: Boolean);
  var
  s:string;
begin
  //Check if the value of the third column is negative,
  //if so change it's font color to Red (clRed).
  try
  s:=Item.SubItems[0];
  if s.Length>0 then
  if s[1]='(' then
    begin
    Sender.Canvas.Font.Color := clYellow;
    Sender.Canvas.Font.Size:=15;
    end;
  except
    on EConvertError do
      next;
  end;
end;

procedure TUPrincipal.ListView6Click(Sender: TObject);
begin
if CheckBox6.Checked then
  begin

  end
  else
  begin
  CheckBox6.Checked:=true;
  CheckBox6.Visible:=true;
  CheckBox2.Checked:=false;
  CheckBox3.Checked:=false;
  CheckBox4.Checked:=false;
  CheckBox5.Checked:=false;
  CheckBox1.Checked:=false;
  CheckBox7.Checked:=false;
  CheckBox8.Checked:=false;
  ActualizarComandasPend;

  end;
end;

procedure TUPrincipal.ListView6CustomDrawSubItem(Sender: TCustomListView;
  Item: TListItem; SubItem: Integer; State: TCustomDrawState;
  var DefaultDraw: Boolean);
  var
  s:string;
begin
  //Check if the value of the third column is negative,
  //if so change it's font color to Red (clRed).
  try
  s:=Item.SubItems[0];
  if s.Length>0 then
  if s[1]='(' then
    begin
    Sender.Canvas.Font.Color := clYellow;
    Sender.Canvas.Font.Size:=15;
    end;
  except
    on EConvertError do
      next;
  end;
end;

procedure TUPrincipal.ListView7Click(Sender: TObject);
begin
if CheckBox7.Checked then
  begin

  end
  else
  begin
  CheckBox7.Checked:=true;
  CheckBox7.Visible:=true;
  CheckBox2.Checked:=false;
  CheckBox3.Checked:=false;
  CheckBox4.Checked:=false;
  CheckBox5.Checked:=false;
  CheckBox6.Checked:=false;
  CheckBox1.Checked:=false;
  CheckBox8.Checked:=false;
  ActualizarComandasPend;

  end;
end;

procedure TUPrincipal.ListView7CustomDrawSubItem(Sender: TCustomListView;
  Item: TListItem; SubItem: Integer; State: TCustomDrawState;
  var DefaultDraw: Boolean);
  var
  s:string;
begin
  //Check if the value of the third column is negative,
  //if so change it's font color to Red (clRed).
  try
  s:=Item.SubItems[0];
  if s.Length>0 then
  if s[1]='(' then
    begin
    Sender.Canvas.Font.Color := clYellow;
    Sender.Canvas.Font.Size:=15;
    end;
  except
    on EConvertError do
      next;
  end;
end;

procedure TUPrincipal.ListView8Click(Sender: TObject);
begin
if CheckBox8.Checked then
  begin

  end
  else
  begin
  CheckBox8.Checked:=true;
   CheckBox8.Visible:=true;
  CheckBox2.Checked:=false;
  CheckBox3.Checked:=false;
  CheckBox4.Checked:=false;
  CheckBox5.Checked:=false;
  CheckBox6.Checked:=false;
  CheckBox7.Checked:=false;
  CheckBox1.Checked:=false;
  ActualizarComandasPend;

  end;
end;


procedure TUPrincipal.ListView8CustomDrawSubItem(Sender: TCustomListView;
  Item: TListItem; SubItem: Integer; State: TCustomDrawState;
  var DefaultDraw: Boolean);
  var
  s:string;
begin
  //Check if the value of the third column is negative,
  //if so change it's font color to Red (clRed).
  try
  s:=Item.SubItems[0];
  if s.Length>0 then
  if s[1]='(' then
    begin
    Sender.Canvas.Font.Color := clYellow;
    Sender.Canvas.Font.Size:=15;
    end;
  except
    on EConvertError do
      next;
  end;
end;

procedure TUPrincipal.PageControl1Change(Sender: TObject);
begin
  ActualizarComandasPend();
  ActualizarComandasComp();
end;

procedure TUPrincipal.SpeedButton1Click(Sender: TObject);
begin
ActualizarComandasPend();
end;

procedure TUPrincipal.SpeedButton1MouseEnter(Sender: TObject);
begin
 ActualizarComandasPend();
end;

procedure TUPrincipal.SpeedButton26Click(Sender: TObject);
var
Itm: TListItem;
begin
ActualizarLista(SpeedButton26.Caption,Shape26.Brush);
end;

procedure TUPrincipal.SpeedButton27Click(Sender: TObject);
begin
ActualizarLista(SpeedButton27.Caption,Shape27.Brush);
end;

procedure TUPrincipal.SpeedButton28Click(Sender: TObject);
begin
ActualizarLista(SpeedButton28.Caption,Shape28.Brush);
end;

procedure TUPrincipal.SpeedButton29Click(Sender: TObject);
begin
ActualizarLista(SpeedButton29.Caption,Shape29.Brush);
end;


procedure TUPrincipal.SpeedButton2Click(Sender: TObject);
begin
ActualizarComandasPend();
end;

procedure TUPrincipal.SpeedButton2MouseEnter(Sender: TObject);
begin
ActualizarComandasPend();
end;

procedure TUPrincipal.SpeedButton30Click(Sender: TObject);
begin
ActualizarLista(SpeedButton30.Caption,Shape30.Brush);
end;

procedure TUPrincipal.SpeedButton31Click(Sender: TObject);
begin
ActualizarLista(SpeedButton31.Caption,Shape31.Brush);
end;

procedure TUPrincipal.SpeedButton32Click(Sender: TObject);
begin
ActualizarLista(SpeedButton32.Caption,Shape32.Brush);
end;

procedure TUPrincipal.SpeedButton33Click(Sender: TObject);
begin
ActualizarLista(SpeedButton33.Caption,Shape33.Brush);
end;

procedure TUPrincipal.SpeedButton34Click(Sender: TObject);
begin
ActualizarLista(SpeedButton34.Caption,Shape34.Brush);
end;

procedure TUPrincipal.SpeedButton35Click(Sender: TObject);
begin
ActualizarLista(SpeedButton35.Caption,Shape35.Brush);
end;

procedure TUPrincipal.SpeedButton36Click(Sender: TObject);
begin
ActualizarLista(SpeedButton36.Caption,Shape36.Brush);
end;

procedure TUPrincipal.SpeedButton37Click(Sender: TObject);
begin
ActualizarLista(SpeedButton37.Caption,Shape37.Brush);
end;

procedure TUPrincipal.SpeedButton38Click(Sender: TObject);
begin
ActualizarLista(SpeedButton38.Caption,Shape38.Brush);
end;

procedure TUPrincipal.SpeedButton39Click(Sender: TObject);
begin
 ActualizarLista(SpeedButton39.Caption,Shape39.Brush);
end;

procedure TUPrincipal.SpeedButton40Click(Sender: TObject);
begin
 ActualizarLista(SpeedButton40.Caption,Shape40.Brush);
end;

procedure TUPrincipal.SpeedButton41Click(Sender: TObject);
begin
 ActualizarLista(SpeedButton41.Caption,Shape41.Brush);
end;

procedure TUPrincipal.SpeedButton42Click(Sender: TObject);
begin
ActualizarLista(SpeedButton42.Caption,Shape42.Brush);
end;

procedure TUPrincipal.SpeedButton43Click(Sender: TObject);
begin
ActualizarLista(SpeedButton43.Caption,Shape43.Brush);
end;

procedure TUPrincipal.SpeedButton44Click(Sender: TObject);
begin
ActualizarLista(SpeedButton44.Caption,Shape44.Brush);
end;

procedure TUPrincipal.SpeedButton45Click(Sender: TObject);
begin
 ActualizarLista(SpeedButton45.Caption,Shape45.Brush);
end;

procedure TUPrincipal.SpeedButton46Click(Sender: TObject);
begin
ActualizarLista(SpeedButton46.Caption,Shape46.Brush);
end;

procedure TUPrincipal.SpeedButton47Click(Sender: TObject);
begin
ActualizarLista(SpeedButton47.Caption,Shape47.Brush);
end;

procedure TUPrincipal.SpeedButton48Click(Sender: TObject);
begin
ActualizarLista(SpeedButton48.Caption,Shape48.Brush);
end;

procedure TUPrincipal.SpeedButton49Click(Sender: TObject);
begin
ActualizarLista(SpeedButton49.Caption,Shape49.Brush);
end;

procedure TUPrincipal.SpeedButton50Click(Sender: TObject);
begin
ActualizarLista(SpeedButton50.Caption,Shape50.Brush);
end;

procedure TUPrincipal.Timer_comandas_compTimer(Sender: TObject);
begin
ActualizarComandasComp();
end;

procedure TUPrincipal.Timer_comandas_pendTimer(Sender: TObject);

begin
ActualizarComandasPend();
end;

procedure TUPrincipal.ActualizarComandasPend();
var
i,numero_comandas:integer;
lista,lista1:TListView;
  Itm: TListItem;
  titulo,titulo1,texto:Tlabel;
  check,check1:TCheckBox;
begin
  //seleccionar las primeras 8 comandas con estado N (No completada)
      //SIN FILTRO DE LINEAS DE ARTICULOS MOSTRAR TODOS
    if Conexion.ExecSQLScalar('select FILTRO_LINEAS_ART from MGTOUCH_SIC_PREFERENCIAS')='N' then
    begin
    qryComandasPend.SQL.Text:='select first 8 distinct vc.venta_comedor_id,clave_cliente,estado,tipo,(select nombre from vendedores where vendedor_id=vc.vendedor_id) from mgtouch_venta_comedor vc  right join mgtouch_venta_det vd'+
    ' on vc.venta_comedor_id=vd.venta_comedor_id where estado=''N'' order by FECHA_HORA_CREACION';
    end
    //CON FILTRO
    else
    begin
    if Conexion.ExecSQLScalar('select count(*) from MGTOUCH_SIC_LINEAS_ART')<>0 then
     begin
      qryFiltro.SQL.Text:='select * from MGTOUCH_SIC_LINEAS_ART';
      qryFiltro.Open();
      qryFiltro.First;
      qryComandasPend.SQL.Text:='select first 8 distinct vc.venta_comedor_id,clave_cliente,estado,tipo,(select nombre from vendedores where vendedor_id=vc.vendedor_id) from mgtouch_venta_comedor vc  right join mgtouch_venta_det vd'+
      ' on vc.venta_comedor_id=vd.venta_comedor_id where estado=''N'' and (select first 1 mgvd.venta_comedor_id from mgtouch_venta_comedor mgv join mgtouch_venta_det mgvd'+
      ' on mgv.venta_comedor_id=mgvd.venta_comedor_id join articulos a'+
      ' on mgvd.articulo_id=a.articulo_id where mgvd.venta_comedor_id=vc.venta_comedor_id and a.linea_articulo_id='+quotedstr(qryFiltro.FieldByName('LINEA_ARTICULO_ID').AsString);
      qryFiltro.Next;
      while not qryFiltro.Eof do
        begin
        qryComandasPendDet.SQL.Text:=qryComandasPendDet.SQL.Text+' or a.linea_articulo_id='+quotedstr(qryFiltro.FieldByName('LINEA_ARTICULO_ID').AsString);
        qryFiltro.Next;
        end;
     qryComandasPend.SQL.Text:=qryComandasPend.SQL.Text+') is not null order by FECHA_HORA_CREACION';
     end;

    end;


qryComandasPend.Open();
qryComandasPend.First;
while not qryComandasPend.Eof do
  begin
    i:=qryComandasPend.RecNo;
    lista:=FindComponent('ListView'+inttostr(i)) as TListView;
    lista.Items.Clear;
    titulo:=FindComponent('Label'+inttostr(i)) as TLabel;
    titulo.Caption:=qryComandasPend.FieldByName('clave_cliente').AsString+'  -  '+qryComandasPend.FieldByName('nombre').AsString;
    titulo.Width:=ListView1.Width;
    texto:=FindComponent('Texto'+inttostr(i)) as TLabel;
    texto.Caption:=qryComandasPend.FieldByName('clave_cliente').AsString;

    //SIN FILTRO DE LINEAS DE ARTICULOS MOSTRAR TODOS
    if Conexion.ExecSQLScalar('select FILTRO_LINEAS_ART from MGTOUCH_SIC_PREFERENCIAS')='N' then
    begin
    qryComandasPendDet.SQL.Text:='select * from MGTOUCH_VENTA_DET where venta_comedor_id=:vid';
    end
    //CON FILTRO
    else
    begin
    if Conexion.ExecSQLScalar('select count(*) from MGTOUCH_SIC_LINEAS_ART')<>0 then
     begin
      qryFiltro.SQL.Text:='select * from MGTOUCH_SIC_LINEAS_ART';
      qryFiltro.Open();
      qryFiltro.First;
          qryComandasPendDet.SQL.Text:='select vd.venta_comedor_id,vd.nombre,vd.unidades,vd.articulo_id,a.linea_articulo_id,vd.comentario from MGTOUCH_VENTA_DET vd'+
        ' join ARTICULOS a on vd.articulo_id=a.articulo_id where venta_comedor_id=:vid and (a.linea_articulo_id='+quotedstr(qryFiltro.FieldByName('LINEA_ARTICULO_ID').AsString);
      qryFiltro.Next;
      while not qryFiltro.Eof do
        begin
        qryComandasPendDet.SQL.Text:=qryComandasPendDet.SQL.Text+' or a.linea_articulo_id='+quotedstr(qryFiltro.FieldByName('LINEA_ARTICULO_ID').AsString);
        qryFiltro.Next;
        end;
        qryComandasPendDet.Sql.Text:=qryComandasPendDet.Sql.Text+')';
     end
     else qryComandasPendDet.SQL.Text:='select * from MGTOUCH_VENTA_DET where venta_comedor_id=:vid';
    end;
    qryComandasPendDet.ParamByName('vid').Value:=qryComandasPend.FieldByName('venta_comedor_id').AsInteger;
    qryComandasPendDet.Open();
    if qryComandasPendDet.RecordCount=0 then
    begin
    titulo.Caption:='';
  //  qryComandasPendDet.Next;
    end
    else
    begin
      while not qryComandasPendDet.Eof do
      begin
      Itm := lista.Items.Add;
      Itm.Caption := qryComandasPendDet.FieldByName('unidades').AsString;
      Itm.SubItems.Add(qryComandasPendDet.FieldByName('nombre').AsString);
            if qryComandasPendDet.FieldByName('COMENTARIO').Value<>null then
       begin

       Itm := lista.Items.Add;
       Itm.Caption := '';
       Itm.SubItems.Add('('+AnsiLowerCase(qryComandasPendDet.FieldByName('COMENTARIO').AsString)+')');
       end;
    //  rectangulo.Canvas.Font.Size  :=10;//set the size of the font
  {  Shape1.Canvas.Font.Color:=clBlack;//set the color of the text   }
   //   rectangulo.Canvas.TextOut(5,y,qryComandasPendDet.FieldByName('unidades').AsString+' - '+qryComandasPendDet.FieldByName('nombre').AsString);
        qryComandasPendDet.Next;
      end;
    end;

      //COLORES EN COMANDAS



  if qryComandasPend.FieldByName('tipo').Value='N' then
    begin
    lista.Color:=clWebDarkgreen;
    titulo.Color:=clBlack;
    Titulo.Font.Color:=clWhite;
    lista.Font.Color:=clWhite;
    end
    else
    begin
      lista.Color:=clWebRoyalBlue;
      titulo.Color:=clBlack;
      Titulo.Font.Color:=clWhite;
      lista.Font.Color:=clWhite;
    end;


  qryComandasPend.Next;
  end;
             //DESPINTAR LOS QUE NO TIENEN COMANDA
    for i := 1 to 8 do
    begin
    check1:=FindComponent('CheckBox'+inttostr(i)) as TCheckBox;
    lista1:=FindComponent('ListView'+inttostr(i)) as TListView;
    titulo1:=FindComponent('Label'+inttostr(i)) as TLabel;
    if check1.Checked=true then
     begin
      lista1.Color:=clSilver;
      titulo1.Color:=clSilver;
      lista1.Font.Color:=clBlack;
     end
     else
    if titulo1.Caption='' then
      begin
      lista1.Color:=clBtnFace;
      titulo1.Color:=clBtnFace;
      check1.Visible:=false;
      end;
    end;
end;

procedure TUPrincipal.ActualizarComandasComp();
var
i,numero_comandas:integer;
boton:TSpeedButton;
fondo:TShape;
tipo:string;
begin
//LIMPIAR BOTONES
for i := 26 to 50 do
  begin
  boton:=FindComponent('SpeedButton'+inttostr(i)) as TSpeedButton;
  fondo:=FindComponent('Shape'+inttostr(i)) as TShape;
  boton.Caption:='';
  fondo.Visible:=false;
  end;

  //seleccionar las primeras 25 comandas con estado N (No completada)
  //sin filtro
      if Conexion.ExecSQLScalar('select FILTRO_LINEAS_ART from MGTOUCH_SIC_PREFERENCIAS')='N' then
    begin
    qryComandasComp.SQL.Text:='select first 25 distinct clave_cliente,estado,tipo from mgtouch_venta_comedor vc  right join mgtouch_venta_det vd'+
    ' on vc.venta_comedor_id=vd.venta_comedor_id where estado=''C'' and fecha_hora_creacion between '+quotedstr(formatdatetime('mm/dd/yyyy',dtpFini.DateTime)+' '+formatdatetime('HH/mm/ss',dtpTini.Time))+' and '+quotedstr(formatdatetime('mm/dd/yyyy',dtpFfin.DateTime)+' '+formatdatetime('HH/mm/ss',dtpTfin.Time))+' order by FECHA_HORA_CREACION';
    end
        //CON FILTRO
    else
    begin
    if Conexion.ExecSQLScalar('select count(*) from MGTOUCH_SIC_LINEAS_ART')<>0 then
     begin
      qryFiltro.SQL.Text:='select * from MGTOUCH_SIC_LINEAS_ART';
      qryFiltro.Open();
      qryFiltro.First;
      qryComandasComp.SQL.Text:='select first 25 distinct vc.venta_comedor_id,clave_cliente,estado,tipo from mgtouch_venta_comedor vc  right join mgtouch_venta_det vd'+
      ' on vc.venta_comedor_id=vd.venta_comedor_id where estado=''C'' and (select first 1 mgvd.venta_comedor_id from mgtouch_venta_comedor mgv join mgtouch_venta_det mgvd'+
      ' on mgv.venta_comedor_id=mgvd.venta_comedor_id join articulos a'+
      ' on mgvd.articulo_id=a.articulo_id where mgvd.venta_comedor_id=vc.venta_comedor_id and a.linea_articulo_id='+quotedstr(qryFiltro.FieldByName('LINEA_ARTICULO_ID').AsString);
      qryFiltro.Next;
      while not qryFiltro.Eof do
        begin
        qryComandasComp.SQL.Text:=qryComandasComp.SQL.Text+' or a.linea_articulo_id='+quotedstr(qryFiltro.FieldByName('LINEA_ARTICULO_ID').AsString);
        qryFiltro.Next;
        end;
     qryComandasComp.SQL.Text:=qryComandasComp.SQL.Text+') is not null and fecha_hora_creacion between '+quotedstr(formatdatetime('mm/dd/yyyy',dtpFini.DateTime)+' '+formatdatetime('HH/mm/ss',dtpTini.Time))+' and '+quotedstr(formatdatetime('mm/dd/yyyy',dtpFfin.DateTime)+' '+formatdatetime('HH/mm/ss',dtpTfin.Time))+' order by FECHA_HORA_CREACION';
     end;

    end;

qryComandasComp.Open();
qryComandasComp.First;
while not qryComandasComp.Eof do
  begin
    i:=25+qryComandasComp.RecNo;
    tipo:=qryComandasComp.FieldByName('tipo').AsString;
    boton:=FindComponent('SpeedButton'+inttostr(i)) as TSpeedButton;
    fondo:=FindComponent('Shape'+inttostr(i)) as TShape;
    boton.Caption:=qryComandasComp.FieldByName('clave_cliente').AsString;
    fondo.Visible:=true;
    //PINTAR BOTONES
    if tipo='N' then
    fondo.Brush.Color:=clWebDarkgreen
    else fondo.Brush.Color:=clWebRoyalBlue;
  qryComandasComp.Next;
  end;
end;

procedure TUPrincipal.LimpiarListas();
var
lista:TListView;
titulo:TLabel;
i:integer;
begin
for i := 1 to 8 do
  begin
  lista:=FindComponent('ListView'+inttostr(i)) as TListView;
  lista.Items.Clear;
  titulo:=FindComponent('Label'+inttostr(i)) as TLabel;
  titulo.Caption:='';
  titulo.Color:=clBtnFace;
  end;
end;

procedure TUPrincipal.ActualizarLista(nombre:String;color:TBrush);
var Itm:TListItem;
venta_comedor_id:Integer;
begin
ListaComanda.Visible:=true;
    venta_comedor_id:=Conexion.ExecSQLScalar('select venta_comedor_id from MGTOUCH_VENTA_COMEDOR where clave_cliente=:cc',[nombre]);
    ListaComanda.Items.Clear;
 //SIN FILTRO DE LINEAS DE ARTICULOS MOSTRAR TODOS
    if Conexion.ExecSQLScalar('select FILTRO_LINEAS_ART from MGTOUCH_SIC_PREFERENCIAS')='N' then
    begin
    qryComandas.SQL.Text:='select venta_det_id,venta_comedor_id,nombre,articulo_id,unidades,comentario,'+
    ' (select nombre from vendedores where vendedor_id=(select vendedor_id from mgtouch_venta_comedor where'+
    ' venta_comedor_id=vd.venta_comedor_id)) as vendedor from MGTOUCH_VENTA_DET vd where venta_comedor_id=:vid';
    end
    //CON FILTRO
    else
    begin
    if Conexion.ExecSQLScalar('select count(*) from MGTOUCH_SIC_LINEAS_ART')<>0 then
     begin
      qryFiltro.SQL.Text:='select * from MGTOUCH_SIC_LINEAS_ART';
      qryFiltro.Open();
      qryFiltro.First;
          qryComandas.SQL.Text:='select vd.venta_comedor_id,vd.nombre,vd.unidades,vd.articulo_id,a.linea_articulo_id,vd.comentario'+
          ' ,(select nombre from vendedores where vendedor_id=(select vendedor_id from mgtouch_venta_comedor where venta_comedor_id=vd.venta_comedor_id)) as vendedor from MGTOUCH_VENTA_DET vd'+
        ' join ARTICULOS a on vd.articulo_id=a.articulo_id where venta_comedor_id=:vid and (a.linea_articulo_id='+quotedstr(qryFiltro.FieldByName('LINEA_ARTICULO_ID').AsString);
      qryFiltro.Next;
      while not qryFiltro.Eof do
        begin
        qryComandas.SQL.Text:=qryComandas.SQL.Text+' or a.linea_articulo_id='+quotedstr(qryFiltro.FieldByName('LINEA_ARTICULO_ID').AsString);
        qryFiltro.Next;
        end;
        qryComandas.Sql.Text:=qryComandas.Sql.Text+')';
     end
     else qryComandas.SQL.Text:='select * from MGTOUCH_VENTA_DET where venta_comedor_id=:vid';
    end;
    qryComandas.ParamByName('vid').Value:=venta_comedor_id;
    qryComandas.Open();
     LabelComanda.Caption:=nombre+'  -  '+qryComandas.FieldByName('vendedor').AsString;
     LabelComanda.Color:=clBlack;
    ListaComanda.Color:=color.Color;
    if qryComandas.RecordCount=0 then
    begin
    LabelComanda.Caption:='';
    ListaComanda.Color:=clBtnFace;
  //  qryComandas.Next;
    end
    else
    begin
      while not qryComandas.Eof do
      begin
      Itm := ListaComanda.Items.Add;
      Itm.Caption := qryComandas.FieldByName('unidades').AsString;
      Itm.SubItems.Add(qryComandas.FieldByName('nombre').AsString);
      if qryComandas.FieldByName('COMENTARIO').Value<>null then
       begin
       Itm := ListaComanda.Items.Add;
       Itm.Caption := '';
       Itm.SubItems.Add('('+AnsiLowerCase(qryComandas.FieldByName('COMENTARIO').AsString)+')');
       end;
       LabelComanda.Width:=ListaComanda.Width;
    //  rectangulo.Canvas.Font.Size  :=10;//set the size of the font
  {  Shape1.Canvas.Font.Color:=clBlack;//set the color of the text   }
   //   rectangulo.Canvas.TextOut(5,y,qryComandas.FieldByName('unidades').AsString+' - '+qryComandas.FieldByName('nombre').AsString);
        qryComandas.Next;
      end;
    end;

end;

end.
