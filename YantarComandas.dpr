program YantarComandas;

uses
  Vcl.Forms,
  Principal in 'Principal.pas' {UPrincipal},
  UConexiones in 'UConexiones.pas' {Conexiones},
  ULogin in 'ULogin.pas' {Login},
  Uselempresa in 'Uselempresa.pas' {SeleccionaEmpresa},
  ULicencia in 'ULicencia.pas',
  UEnvVars in 'UEnvVars.pas',
  WbemScripting_TLB in 'WbemScripting_TLB.pas',
  UPreferencias in 'UPreferencias.pas' {Preferencias},
  UContrasena in 'UContrasena.pas' {Contrasena},
  UContrasenaCambiar in 'UContrasenaCambiar.pas' {ContrasenaCambiar};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
 // Application.CreateForm(TUPrincipal, UPrincipal);
  //Application.CreateForm(TConexiones, Conexiones);
  Application.CreateForm(TLogin, Login);
  Application.CreateForm(TPreferencias, Preferencias);
  Application.CreateForm(TContrasenaCambiar, ContrasenaCambiar);
  // Application.CreateForm(TContraseña, Contraseña);
  //  Application.CreateForm(TSeleccionaEmpresa, SeleccionaEmpresa);
  Application.Run;
end.
