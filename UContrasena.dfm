﻿object Contrasena: TContrasena
  Left = 0
  Top = 0
  Caption = 'Contrasena'
  ClientHeight = 91
  ClientWidth = 259
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 104
    Height = 13
    Caption = 'Ingresar Contrase'#241'a:'
  end
  object txtContraseña: TEdit
    Left = 8
    Top = 27
    Width = 246
    Height = 21
    PasswordChar = '*'
    TabOrder = 0
    OnKeyDown = txtContraseñaKeyDown
  end
  object btnAceptar: TButton
    Left = 37
    Top = 54
    Width = 75
    Height = 25
    Caption = 'Aceptar'
    TabOrder = 1
    OnClick = btnAceptarClick
    OnKeyDown = btnAceptarKeyDown
  end
  object btnCancelar: TButton
    Left = 118
    Top = 54
    Width = 75
    Height = 25
    Caption = 'Cancelar'
    TabOrder = 2
    OnClick = btnCancelarClick
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'DriverID=FB'
      'User_Name=sysdba'
      'Password=masterkey')
    Left = 207
    Top = 56
  end
end
