object ContrasenaCambiar: TContrasenaCambiar
  Left = 0
  Top = 0
  Caption = 'Cambiar Contrase'#241'a'
  ClientHeight = 107
  ClientWidth = 326
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 24
    Top = 11
    Width = 94
    Height = 13
    Caption = 'Contrase'#241'a Nueva:'
  end
  object Label2: TLabel
    Left = 8
    Top = 51
    Width = 110
    Height = 13
    Caption = 'Confirmar Contrase'#241'a:'
  end
  object txtContra: TEdit
    Left = 124
    Top = 8
    Width = 193
    Height = 21
    PasswordChar = '*'
    TabOrder = 0
  end
  object txtConfirmar: TEdit
    Left = 124
    Top = 48
    Width = 193
    Height = 21
    PasswordChar = '*'
    TabOrder = 1
  end
  object btnAceptar: TButton
    Left = 80
    Top = 75
    Width = 75
    Height = 25
    Caption = 'Aceptar'
    TabOrder = 2
    OnClick = btnAceptarClick
  end
  object btnCancelar: TButton
    Left = 161
    Top = 75
    Width = 75
    Height = 25
    Caption = 'Cancelar'
    TabOrder = 3
    OnClick = btnCancelarClick
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'DriverID=FB'
      'User_Name=sysdba'
      'Password=masterkey')
    Left = 207
    Top = 56
  end
end
