object Preferencias: TPreferencias
  Left = 0
  Top = 0
  Caption = 'Preferencias'
  ClientHeight = 262
  ClientWidth = 527
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 13
    Width = 25
    Height = 13
    Caption = 'Linea'
  end
  object CheckBox1: TCheckBox
    Left = 350
    Top = 8
    Width = 169
    Height = 17
    Caption = 'Solo mostrar lineas de articulo'
    TabOrder = 0
    OnClick = CheckBox1Click
  end
  object grdLineas: TDBGrid
    Left = 8
    Top = 32
    Width = 497
    Height = 185
    DataSource = dsLineas
    Options = [dgEditing, dgAlwaysShowEditor, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object cbLinea: TDBLookupComboBox
    Left = 39
    Top = 8
    Width = 145
    Height = 21
    DropDownRows = 20
    DropDownWidth = 400
    KeyField = 'LINEA_ARTICULO_ID'
    ListField = 'NOMBRE'
    ListSource = dsLineasArticulos
    TabOrder = 2
  end
  object btnAgregar: TButton
    Left = 190
    Top = 5
    Width = 75
    Height = 25
    Caption = 'Agregar'
    TabOrder = 3
    OnClick = btnAgregarClick
  end
  object btnRemover: TButton
    Left = 8
    Top = 223
    Width = 75
    Height = 25
    Caption = 'Remover'
    TabOrder = 4
    OnClick = btnRemoverClick
  end
  object Conexion: TFDConnection
    Params.Strings = (
      'User_Name=sysdba'
      'Password=masterkey'
      'Database=C:\Microsip datos 2020\VET LOYA PRUEBA 2019.FDB'
      'DriverID=FB')
    Left = 480
    Top = 152
  end
  object qryLineas: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      
        'select mgsla.linea_articulo_id,la.nombre from MGTOUCH_SIC_LINEAS' +
        '_ART mgsla'
      
        'left join lineas_articulos la on mgsla.linea_articulo_id=la.line' +
        'a_articulo_id'
      '')
    Left = 480
    Top = 56
    object qryLineasLINEA_ARTICULO_ID: TIntegerField
      FieldName = 'LINEA_ARTICULO_ID'
      Origin = 'LINEA_ARTICULO_ID'
      Visible = False
    end
    object qryLineasNOMBRE: TStringField
      AutoGenerateValue = arDefault
      DisplayLabel = 'Linea de articulos'
      FieldName = 'NOMBRE'
      Origin = 'NOMBRE'
      ProviderFlags = []
      ReadOnly = True
      Size = 50
    end
  end
  object dsLineas: TDataSource
    DataSet = qryLineas
    Left = 392
    Top = 136
  end
  object qryLineasArticulos: TFDQuery
    Connection = Conexion
    SQL.Strings = (
      'select * from lineas_articulos')
    Left = 272
    Top = 64
  end
  object dsLineasArticulos: TDataSource
    DataSet = qryLineasArticulos
    Left = 240
    Top = 104
  end
end
