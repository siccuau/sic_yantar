unit UContrasena;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,UPreferencias,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async,
  FireDAC.Phys, FireDAC.Phys.FB, FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait,
  Data.DB, FireDAC.Comp.Client,UContrasenaCambiar;

type
  TContrasena = class(TForm)
    Label1: TLabel;
    txtContrase�a: TEdit;
    btnAceptar: TButton;
    btnCancelar: TButton;
    Conexion: TFDConnection;
    procedure btnAceptarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure txtContrase�aKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnAceptarKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    nombre_boton:string;
  end;

var
  Contrasena: TContrasena;

implementation

{$R *.dfm}

procedure TContrasena.btnAceptarClick(Sender: TObject);
var
  pref : TPreferencias;
  contr:TContrasenaCambiar;
begin
if txtContrase�a.Text=Conexion.ExecSQLScalar('select CONTRASENA from MGTOUCH_SIC_CONTRASENA') then
 begin
  if nombre_boton='Preferencias' then
    begin
      pref := TPreferencias.Create(nil);
      pref.Conexion.Params := Conexion.Params;
      pref.Conexion.Open;
      pref.ShowModal;
      self.Close;
  end
  else
    if nombre_boton='Devolver' then
  begin
  ModalResult:=mrOk;
  end
  else
    if nombre_boton='Cambiar Contrasena' then
    begin
      contr := TContrasenaCambiar.Create(nil);
      contr.Conexion.Params := Conexion.Params;
      contr.Conexion.Open;
      contr.ShowModal;
      self.Close;
  end
 end
 else ShowMessage('Contrase�a Incorrecta.');
end;

procedure TContrasena.btnAceptarKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
  var
  pref : TPreferencias;
begin
if key=VK_RETURN then
 begin
  if txtContrase�a.Text=Conexion.ExecSQLScalar('select CONTRASENA from MGTOUCH_SIC_CONTRASENA') then
 begin
  if nombre_boton='Preferencias' then
    begin
      pref := TPreferencias.Create(nil);
      pref.Conexion.Params := Conexion.Params;
      pref.Conexion.Open;
      pref.ShowModal;
      self.Close;
  end
  else
    if nombre_boton='Devolver' then
  begin
  ModalResult:=mrOk;
  end;
 end
 else ShowMessage('Contrase�a Incorrecta.');
 end;
end;

procedure TContrasena.btnCancelarClick(Sender: TObject);
begin
ModalResult:=mrCancel;
end;

procedure TContrasena.txtContrase�aKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if Key=VK_RETURN then
 FocusControl(btnAceptar);
end;

end.
