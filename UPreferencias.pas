unit UPreferencias;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, FireDAC.Stan.Intf, FireDAC.Stan.Option,
  FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf, FireDAC.Stan.Def,
  FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.FB,
  FireDAC.Phys.FBDef, FireDAC.VCLUI.Wait, Data.DB, FireDAC.Comp.Client,
  FireDAC.Stan.Param, FireDAC.DatS, FireDAC.DApt.Intf, FireDAC.DApt,
  FireDAC.Comp.DataSet, Vcl.Grids, Vcl.DBGrids, Vcl.StdCtrls, Vcl.DBCtrls;

type
  TPreferencias = class(TForm)
    Conexion: TFDConnection;
    CheckBox1: TCheckBox;
    grdLineas: TDBGrid;
    qryLineas: TFDQuery;
    dsLineas: TDataSource;
    Label1: TLabel;
    qryLineasArticulos: TFDQuery;
    dsLineasArticulos: TDataSource;
    cbLinea: TDBLookupComboBox;
    qryLineasLINEA_ARTICULO_ID: TIntegerField;
    qryLineasNOMBRE: TStringField;
    btnAgregar: TButton;
    btnRemover: TButton;
    procedure FormShow(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure btnAgregarClick(Sender: TObject);
    procedure btnRemoverClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Preferencias: TPreferencias;

implementation

{$R *.dfm}

procedure TPreferencias.btnAgregarClick(Sender: TObject);
begin
  if Conexion.ExecSQLScalar('select count(linea_articulo_id) from MGTOUCH_SIC_LINEAS_ART where linea_articulo_id=:aid',[cbLinea.KeyValue])=0 then
  begin
  Conexion.ExecSQL('insert into MGTOUCH_SIC_LINEAS_ART(linea_articulo_id) values(:aid)',[cbLinea.KeyValue]);
  Conexion.Commit;
  grdLineas.DataSource.DataSet.Refresh;
  end
  else
  begin
  ShowMessage('Esta linea ya esta agregada.');
  end;


end;


procedure TPreferencias.btnRemoverClick(Sender: TObject);
begin
  if qryLineas.RecordCount>0 then
  begin
    Conexion.ExecSQL('delete from MGTOUCH_SIC_LINEAS_ART where LINEA_ARTICULO_ID=:aid',[qryLineasLINEA_ARTICULO_ID.Value]);
    Conexion.Commit;
    grdLineas.DataSource.DataSet.Refresh;
  end;
end;

procedure TPreferencias.CheckBox1Click(Sender: TObject);
begin
if CheckBox1.Checked then
 Conexion.ExecSQL('update MGTOUCH_SIC_PREFERENCIAS set FILTRO_LINEAS_ART=''S''')
 else
 Conexion.ExecSQL('update MGTOUCH_SIC_PREFERENCIAS set FILTRO_LINEAS_ART=''N''');
 Conexion.Commit;
end;

procedure TPreferencias.FormShow(Sender: TObject);
begin
qryLineasArticulos.Open();
qryLineas.SQL.Text:='select mgsla.linea_articulo_id,la.nombre from MGTOUCH_SIC_LINEAS_ART mgsla'+
' left join lineas_articulos la on mgsla.linea_articulo_id=la.linea_articulo_id';
qryLineas.Open();
 if Conexion.ExecSQLScalar('select FILTRO_LINEAS_ART from MGTOUCH_SIC_PREFERENCIAS')='S' then
    CheckBox1.Checked:=true
    else
    CheckBox1.Checked:=false;

cbLinea.KeyValue:=Conexion.ExecSQLScalar('select first 1 linea_articulo_id from lineas_articulos');
end;

end.
